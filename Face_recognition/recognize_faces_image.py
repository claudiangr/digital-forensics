import face_recognition
import argparse
import pickle
import cv2

# Parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-e", "--encodings", required=True,
	help="path to serialized db of facial encodings")
ap.add_argument("-i", "--image", required=True,
	help="path to input image")
ap.add_argument("-d", "--detection-method", type=str, default="hog",
	help="face detection model to use: either `hog` or `cnn`")
args = vars(ap.parse_args())

# Load the known faces and embeddings
print("Loading encodings...")
data = pickle.loads(open(args["encodings"], "rb").read())

# Load the input image and convert it from BGR to RGB
image = cv2.imread(args["image"])
rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# Detect the coordinates of the bounding boxes corresponding to each face in the input image, next compute the facial embedding for each face
print("Recognizing faces...")
boxes = face_recognition.face_locations(rgb, model = args["detection_method"])
encodings = face_recognition.face_encodings(rgb, boxes)

# Initialize the list of names for each face detected
names = []

# Loop over the facial encodings
for encoding in encodings:
	# Try attempt to match each face in the input image to our known encodings
	matches = face_recognition.compare_faces(data["encodings"], encoding)
	name = "Unknown"

	# Check if there is a match
	if True in matches:
		# Find the indexes of matched faces then initialize a dictionary to count the total # of times each face was matched
		matchedIdxs = [i for (i, b) in enumerate(matches) if b]
		counts = {}

		# Loop over the matched indexes and maintain a count for each recognized face face
		for i in matchedIdxs:
			name = data["names"][i]
			counts[name] = counts.get(name, 0) + 1

		# Determine the recognized face with the largest # of votes
		name = max(counts, key=counts.get)
	
	# Update the list of names
	names.append(name)

# Loop over the recognized faces
for ((top, right, bottom, left), name) in zip(boxes, names):
	# Draw the predicted face name on the image
	cv2.rectangle(image, (left, top), (right, bottom), (202,255,112), 2)
	y = top - 15 if top - 15 > 15 else top + 15
	cv2.putText(image, name, (left, y), cv2.FONT_HERSHEY_DUPLEX, 0.75, (202,255,112), 2)

# Show the output image
cv2.imshow("Image", image)
cv2.waitKey(0)
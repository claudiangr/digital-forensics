import face_recognition
import argparse
import imutils
import pickle
import time
import cv2

# Parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-e", "--encodings", required=True,
	help="path to serialized db of facial encodings")
ap.add_argument("-i", "--input", required=True,
	help="path to input video")
ap.add_argument("-o", "--output", type=str,
	help="path to output video")
ap.add_argument("-y", "--display", type=int, default=1,
	help="whether or not to display output frame to screen")
ap.add_argument("-d", "--detection-method", type=str, default="hog",
	help="face detection model to use: either `hog` or `cnn`")
args = vars(ap.parse_args())

# Load the known faces and embeddings
print("Loading encodings...")
data = pickle.loads(open(args["encodings"], "rb").read())

# Initialize the pointer to the video file and the video writer
print("Processing video...")
stream = cv2.VideoCapture(args["input"])
writer = None

# Loop over frames from the video file stream
while True:
	# Grab the next frame
	(grabbed, frame) = stream.read()

	# If the frame was not grabbed, then the end of the video is reached
	if not grabbed:
		break

	# Convert the input frame from BGR to RGB then resize it to have a width of 750px (to speedup processing)
	rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
	rgb = imutils.resize(frame, width=750)
	r = frame.shape[1] / float(rgb.shape[1])

	# Detect the (x, y)-coordinates of the bounding boxes corresponding to each face in the input frame, then compute  the facial embeddings for each face
	boxes = face_recognition.face_locations(rgb, model=args["detection_method"])
	encodings = face_recognition.face_encodings(rgb, boxes)
	names = []

	# Loop over the facial embeddings
	for encoding in encodings:
		# Attempt to match each face in the input image to our known encodings
		matches = face_recognition.compare_faces(data["encodings"],
			encoding)
		name = "Unknown"

		# check to see if we have found a match
		if True in matches:
		# Find the indexes of matched faces then initialize a dictionary to count the total # of times each face was matched
			matchedIdxs = [i for (i, b) in enumerate(matches) if b]
			counts = {}

			# loop over the matched indexes and maintain a count for
			# each recognized face face
			for i in matchedIdxs:
				name = data["names"][i]
				counts[name] = counts.get(name, 0) + 1

			# Determine the recognized face with the largest # of votes
			name = max(counts, key=counts.get)
		
		# update the list of names
		names.append(name)

	# Loop over the recognized faces
	for ((top, right, bottom, left), name) in zip(boxes, names):
		# Rescale the face coordinates
		top = int(top * r)
		right = int(right * r)
		bottom = int(bottom * r)
		left = int(left * r)

		# Draw the predicted face name on the image
		cv2.rectangle(frame, (left, top), (right, bottom), (202,255,112), 2)
		y = top - 15 if top - 15 > 15 else top + 15
		cv2.putText(frame, name, (left, y), cv2.FONT_HERSHEY_DUPLEX, 0.75, (202,255,112), 2)

	# If the video writer is None *AND* we are supposed to write the output video to disk initialize the writer
	if writer is None and args["output"] is not None:
		fourcc = cv2.VideoWriter_fourcc(*"MJPG")
		writer = cv2.VideoWriter(args["output"], fourcc, 24, (frame.shape[1], frame.shape[0]), True)

	# If the writer is not None, write the frame with recognized faces t0 disk
	if writer is not None:
		writer.write(frame)

	# Check to see if we are supposed to display the output frame to the screen
	if args["display"] > 0:
		cv2.imshow("Frame", frame)
		key = cv2.waitKey(1) & 0xFF

		# If the `q` key is pressed, break from the loop
		if key == ord("q"):
			break

# Close the video file pointers
stream.release()

# Check to see if the video writer point needs to be released
if writer is not None:
	writer.release()

                                          README


These instructions will make it easier to set the project up and running it on your local machine for development and testing purposes. 


--------------------------
Before Running The Project
--------------------------

In order to use the facial recognition system, apart from Python and OpenCv, you need to install two libraries that will be used: 
- dlib
- face_recognition
Furthermore, you’ll also need to install the imutils package of convenience functions.




-------------------
Running The Project
-------------------

When running the project, the order in which scripts need to be executed is:
I. encode_faces.py          to extract encodings from the dataset of faces
II.  
    A. recognize_faces_image.py         to perform facial recognition in images
    B. recognize_faces_video.py         to perform facial recognition in video streams

    

----------------------
Command Line Arguments
----------------------

I.
The command line to be used when running the encode_faces.py script is:

Example:  python encode_faces.py --dataset PeopleFace --encodings encodings.pickle

Where:
--dataset is the path to the input PropleFace dataset,
--encodings is the path to serialized db of facial encodings,
NB: --detection-method is the face detection model to use: either `hog` or `cnn`. Hog is the default one (can be changed inside the script).


II.A
The command line to be used when running the recognize_faces_image.py script is:

Example1:  python recognize_faces_image.py --encodings encodings.pickle --image Images/gwyneth.jpg

Example2:  python recognize_faces_image.py --encodings encodings.pickle --image Images/bruce.jpg

Where:
--encodings is the path to serialized db of facial encodings (encodings.pickle file),
--image is the path to the input image,
NB: --detection-method is the face detection model to use: either `hog` or `cnn`. Hog is the default one (can be changed inside the script).



II.B
The command line to be used when running the recognize_faces_video.py  script is:

Example1: python recognize_faces_video.py --encodings encodings.pickle --input Videos/alyssa.mp4 --output Output/alyssa_output.avi --display 0

Example2: python recognize_faces_video.py --encodings encodings.pickle --input Videos/hjackman.mp4 --output Output/hjackman_output.avi --display 0

Where:
--encodings is the path to serialized db of facial encodings (encodings.pickle file),
--input is the path to input video,
--output is the path to the folder wherw the output video will be saved,
--display indicates whether or not to display output frame to screen, a value of 1 will display the frame to screen while 0 will not (default value is 1),
NB: --detection-method is the face detection model to use: either `hog` or `cnn`. Hog is the default one (can be changed inside the script).
